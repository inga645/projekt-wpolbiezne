#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/ipc.h> 
#include <sys/msg.h>     
#include <string.h>
#include <signal.h>


struct msgbuf{
	long type;
	char mes[200];
	char czas[30];
	int numerklienta;
	char odbiorca[10];
	char nadawca[10];
	int nrread;
	int log[9];
} msgwr,msgrd,msgrd1;

void printmenu(){
	printf("(1) logout\n(2) zalogowani uzytkownicy\n");
	printf("(3) czlonkowie grupy\n(4) dolacz do grupy\n");
	printf("(5) opusc grupe\n(6) lista grup\n");
	printf("(7) wyslij wiadomosc do uzytkownika\n(8) wyslij wiadomosc do grupy\n");

}

int main(int argc, char* argv[]){
	int readkey;
	readkey = getpid();

	int idwrite, idread;
		//kolejka do wysylania komunikatow: key=2000
		idwrite = msgget(2000,0644|IPC_CREAT);
		//kolejka do odbierania komunikatow: key=pid
		idread = msgget(readkey,0644|IPC_CREAT);

	msgwr.nrread = readkey;

	int r;
	msgwr.type = 1;
	char login[10], haslo[10];
	do{
		printf("Podaj login: \n");
		scanf("%s",login);
		msgwr.numerklienta = atoi(&login[4]);
		printf("Podaj haslo: \n");
		scanf("%s",haslo);
		strcpy(msgwr.mes,login);
		strcat(msgwr.mes,haslo);
		strcpy(msgwr.nadawca,login);
		msgsnd(idwrite,&msgwr,sizeof(struct msgbuf)-sizeof(long),0);
		r = msgrcv(idread,&msgrd,sizeof(struct msgbuf)-sizeof(long),1,0);
		if(r!=-1) printf("%s\n",msgrd.mes);
	}while(strcmp(msgrd.mes,"Zalogowano poprawnie\n")!=0);


	int x = fork();

	if(x==0){
		while(1){
			msgrcv(idread,&msgrd1,sizeof(struct msgbuf)-sizeof(long),10,0);
			printf("\nNEW MESSAGE\nFROM: %s\n TO: %s\nTIME: %s MESSAGE: %s\n",msgrd1.nadawca,msgrd1.odbiorca,msgrd1.czas,msgrd1.mes);
			sleep(3);
		}
	}
	else{
		while(1){
			printmenu();
			int operacja;
			scanf("%d",&operacja);

			char str[10];
			char to[10], mess[200];

			switch(operacja){
			case 1: //logout
				msgwr.type = 2;
				msgsnd(idwrite,&msgwr,sizeof(struct msgbuf)-sizeof(long),0);
				r = msgrcv(idread,&msgrd,sizeof(struct msgbuf)-sizeof(long),2,0);
				if(r!=-1) printf("%s\n",msgrd.mes);
				msgctl(idwrite,IPC_RMID,NULL);
				msgctl(idread,IPC_RMID,NULL);
				kill(x,SIGTERM);
				return 0;
				break;

			case 2: //zalogowani
				msgwr.type = 3;
				msgsnd(idwrite,&msgwr,sizeof(struct msgbuf)-sizeof(long),0);
				r = msgrcv(idread,&msgrd,sizeof(struct msgbuf)-sizeof(long),3,0);
				if(r!=-1){
					printf("Zalogowani uzytkownicy: \n");
					int i;
					for(i = 0; i < 9; i++){
						if(msgrd.log[i] == 1)
							printf("test%d\n",i+1);
					}
				}
				break;

			case 3: //zapisani do grupy
				msgwr.type = 4;
				printf("Podaj nazwe grupy: \n");
				scanf("%s",str);
				strcpy(msgwr.mes,str);
				msgsnd(idwrite,&msgwr,sizeof(struct msgbuf)-sizeof(long),0);
				r = msgrcv(idread,&msgrd,sizeof(struct msgbuf)-sizeof(long),4,0);
				if(r!=-1){
					printf("Czlonkowie grupy %s:\n",str);
					int i;
					for(i = 0; i < 9; i++){
						if(msgrd.log[i] == 1)
							printf("test%d\n",i+1);
					}
				}
				break;

			case 4://dolacz do grupy
				msgwr.type = 5;
				printf("Podaj nazwe grupy: \n");
				scanf("%s",str);
				strcpy(msgwr.mes,str);
				msgsnd(idwrite,&msgwr,sizeof(struct msgbuf)-sizeof(long),0);
				r = msgrcv(idread,&msgrd,sizeof(struct msgbuf)-sizeof(long),5,0);
				if(r!=-1) printf("%s\n",msgrd.mes);
				break;

			case 5: // opusc grupe
				msgwr.type = 6;
				printf("Podaj nazwe grupy: \n");
				scanf("%s",str);
				strcpy(msgwr.mes,str);
				msgsnd(idwrite,&msgwr,sizeof(struct msgbuf)-sizeof(long),0);
				r = msgrcv(idread,&msgrd,sizeof(struct msgbuf)-sizeof(long),6,0);
				if(r!=-1) printf("%s\n",msgrd.mes);
				break;

			case 6: // pokaz grupy
				msgwr.type = 7;
				msgsnd(idwrite,&msgwr,sizeof(struct msgbuf)-sizeof(long),0);
				r = msgrcv(idread,&msgrd,sizeof(struct msgbuf)-sizeof(long),7,0);
				if(r!=-1){
					printf("Dostepne grupy: \n");
					printf("%s\n",msgrd.mes);
				}
				break;

			case 7: //wiadomosc
				msgwr.type = 8;
				printf("Podaj nazwe uzytkownika: \n");
				scanf("%s",str);
				strcpy(msgwr.odbiorca,str);
				printf("Tresc wiadomosci (bez spacji): \n");
				scanf("%s",mess);
				strcpy(msgwr.mes,mess);
				msgsnd(idwrite,&msgwr,sizeof(struct msgbuf)-sizeof(long),0);
				r = msgrcv(idread,&msgrd,sizeof(struct msgbuf)-sizeof(long),8,0);
				printf("message: %s\n",msgwr.mes);
				if(r!=-1){
					printf("%s\n",msgrd.mes);
				}
				break;

			case 8: //wiadomosc do grupy
				msgwr.type = 9;
				printf("Podaj nazwe grupy: \n");
				scanf("%s",to);
				strcpy(msgwr.odbiorca,to);
				printf("Tresc wiadomosci (bez spacji): \n");
				scanf("%s",mess);
				strcpy(msgwr.mes,mess);
				msgsnd(idwrite,&msgwr,sizeof(struct msgbuf)-sizeof(long),0);
				r = msgrcv(idread,&msgrd,sizeof(struct msgbuf)-sizeof(long),9,0);
				printf("message: %s\n",msgwr.mes);
				if(r!=-1){
					printf("%s\n",msgrd.mes);
				}
				break;

			}
		}
		}
		return 0;
	}
