#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/ipc.h> 
#include <sys/msg.h>           
#include <fcntl.h> 
#include <string.h>
#include <time.h>

struct msgbuf{
	long type;
	char mes[200];
	char czas[30];
	int numerklienta;
	char odbiorca[10];
	char nadawca[10];
	int nrread;
	int log[9];
} msgwr, msgrd;

int zalogowani[9] = {0,0,0,0,0,0,0,0,0};
int group1[9] = {0,0,0,0,0,0,0,0,0};
int group2[9] = {0,0,0,0,0,0,0,0,0};
int group3[9] = {0,0,0,0,0,0,0,0,0};
char hasla[9][10] = {"","","","","","","","",""};
char login[9][10] = {"","","","","","","","",""};
long keys[9] = {0,0,0,0,0,0,0,0,0};

void loadconfig(){
	FILE* plik;
	plik = fopen("./config.txt","r");
	int n;
	char str[10];
	char has[10];
	int g1,g2,g3;
	for(n=0; n<9; n++){
		fscanf(plik, "%s %s %d %d %d", str, has,  &g1, &g2, &g3);
		group1[n] = g1;
		group2[n] = g2;
		group3[n] = g3;
		strcpy(hasla[n],has);
		strcpy(login[n],str);
	}
	fclose(plik);
}

int main(int argc, char* argv[]){

	loadconfig();
	int idread,idwrite;
	//kolejka do odczytywania komunikatow od klientow
	idread = msgget(2000,0644| IPC_CREAT);


	while(1){
		int i;
		msgrcv(idread,&msgrd,sizeof(struct msgbuf)-sizeof(long),0,0);

		switch(msgrd.type){
		case 1: //login
			msgwr.type = 1;
			char nazwa[20];
			idwrite = msgget(msgrd.nrread,0644| IPC_CREAT);
			keys[msgrd.numerklienta-1] = idwrite;
			strcpy(nazwa, login[msgrd.numerklienta-1]);
			strcat(nazwa, hasla[msgrd.numerklienta-1]);
			if(strcmp(nazwa,msgrd.mes)==0){
				strcpy(msgwr.mes,"Zalogowano poprawnie\n");
				zalogowani[msgrd.numerklienta-1] = 1;
			} else {
				strcpy(msgwr.mes,"Zly login lub haslo\n");
			}
			msgsnd(keys[msgrd.numerklienta-1],&msgwr,sizeof(struct msgbuf)-sizeof(long),0);
			break;

		case 2: //logout
			msgwr.type = 2;
			zalogowani[msgrd.numerklienta-1] = 0;
			strcpy(msgwr.mes,"Wylogowano poprawnie\n");
			msgsnd(keys[msgrd.numerklienta-1],&msgwr,sizeof(struct msgbuf)-sizeof(long),0);
			msgctl(keys[msgrd.numerklienta-1],IPC_RMID,NULL);
			break;

		case 3: //zalogowani
			msgwr.type = 3;
			for(i=0; i<9;i++){
				msgwr.log[i] = zalogowani[i];
			}
			msgsnd(keys[msgrd.numerklienta-1],&msgwr,sizeof(struct msgbuf)-sizeof(long),0);
			break;

		case 4: //zapisani do grupy
			msgwr.type = 4;
			if(strcmp(msgrd.mes,"group1")==0){
				for(i=0; i<9;i++){
					msgwr.log[i] = group1[i];
				}
			}else if(strcmp(msgrd.mes,"group2")==0){
				for(i=0; i<9;i++){
					msgwr.log[i] = group2[i];
				}
			}else if(strcmp(msgrd.mes,"group3")==0){
				for(i=0; i<9;i++){
					msgwr.log[i] = group3[i];
				}
			}
			msgsnd(keys[msgrd.numerklienta-1],&msgwr,sizeof(struct msgbuf)-sizeof(long),0);
			break;

		case 5://dolacz do grupy
			msgwr.type = 5;
			if(strcmp(msgrd.mes,"group1")==0){
				group1[msgrd.numerklienta-1]=1;
			}else if(strcmp(msgrd.mes,"group2")==0){
				group2[msgrd.numerklienta-1]=1;
			}else if(strcmp(msgrd.mes,"group3")==0){
				group3[msgrd.numerklienta-1]=1;
			}
			strcpy(msgwr.mes,"Dolaczyles do grupy\n");
			msgsnd(keys[msgrd.numerklienta-1],&msgwr,sizeof(struct msgbuf)-sizeof(long),0);
			break;

		case 6: // opusc grupe
			msgwr.type = 6;
			if(strcmp(msgrd.mes,"group1") == 0){
				group1[msgrd.numerklienta-1] = 0;
			}else if(strcmp(msgrd.mes,"group2") == 0){
				group2[msgrd.numerklienta-1] = 0;
			}else if(strcmp(msgrd.mes,"group3") == 0){
				group3[msgrd.numerklienta-1] = 0;
			}
			strcpy(msgwr.mes,"Nie jestes czlonkiem grupy\n");
			msgsnd(keys[msgrd.numerklienta-1],&msgwr,sizeof(struct msgbuf)-sizeof(long),0);
			break;

		case 7: // pokaz grupy
			msgwr.type = 7;
			strcpy(msgwr.mes,"group1\ngroup2\ngroup3\n");
			msgsnd(keys[msgrd.numerklienta-1],&msgwr,sizeof(struct msgbuf)-sizeof(long),0);
			break;

		case 8: //wiadomosc do uzytkownika
			msgwr.type = 10;
			time_t now;
			struct tm *nowinfo;
			time(&now);
			nowinfo = localtime(&now);
			strcpy(msgwr.czas,asctime(nowinfo));
			//printf("%s\n",asctime(nowinfo));
			strcpy(msgwr.mes,msgrd.mes);
			strcpy(msgwr.nadawca,msgrd.nadawca);
			strcpy(msgwr.odbiorca,msgrd.odbiorca);
			int num = atoi(&msgrd.odbiorca[4]);
			if(zalogowani[num-1]==1){
				msgsnd(keys[num-1],&msgwr,sizeof(struct msgbuf)-sizeof(long),0);
				msgwr.type = 8;
				strcpy(msgwr.mes,"Wiadomosc wyslano\n");
				msgsnd(keys[msgrd.numerklienta-1],&msgwr,sizeof(struct msgbuf)-sizeof(long),0);
			} else {
				msgwr.type = 8;
				strcpy(msgwr.mes,"Nie mozna wyslac wiadomosci do niezalogowanego uzytkownika\n");
				msgsnd(keys[msgrd.numerklienta-1],&msgwr,sizeof(struct msgbuf)-sizeof(long),0);
			}
			break;

		case 9: //wiadomosc do grupy
			msgwr.type = 10;
			int i;
			time(&now);
			nowinfo = localtime(&now);
			strcpy(msgwr.czas,asctime(nowinfo));
			strcpy(msgwr.mes,msgrd.mes);
			strcpy(msgwr.nadawca,msgrd.nadawca);
			strcpy(msgwr.odbiorca,msgrd.odbiorca);
			if(strcmp(msgrd.odbiorca,"group1")==0){
				for(i=0;i<9;i++){
					if(group1[i]==1 && zalogowani[i]==1){
						msgsnd(keys[i],&msgwr,sizeof(struct msgbuf)-sizeof(long),0);
					}
				}
			}else if(strcmp(msgrd.odbiorca,"group2")==0){
				for(i=0;i<9;i++){
					if(group2[i]==1 && zalogowani[i]==1){
						msgsnd(keys[i],&msgwr,sizeof(struct msgbuf)-sizeof(long),0);
					}
				}
			}else if(strcmp(msgrd.odbiorca,"group3")==0){
				for(i=0;i<9;i++){
					if(group3[i]==1 && zalogowani[i]==1){
						msgsnd(keys[i],&msgwr,sizeof(struct msgbuf)-sizeof(long),0);
					}
				}
			}
			msgwr.type = 9;
			strcpy(msgwr.mes,"Wiadomosc wyslana do zalogowanych uzytkownikow grupy\n");
			msgsnd(keys[msgrd.numerklienta-1],&msgwr,sizeof(struct msgbuf)-sizeof(long),0);
			break;

		}
	}

	return 0;
}
